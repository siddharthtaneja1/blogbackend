package com.Blog.Blog.Repository;

import com.Blog.Blog.Model.Blogs;
import com.Blog.Blog.Model.Comments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentsRepository extends JpaRepository<Comments,Long> {

    List<Comments> findAllByBlogs(Optional<Blogs> comments);
}
