package com.Blog.Blog.Controller;

import com.Blog.Blog.Model.Comments;
import com.Blog.Blog.Repository.CommentsRepository;
import com.Blog.Blog.Service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/comment")
@CrossOrigin(origins = "http://localhost:4200",allowedHeaders = "*")
public class CommentController {

    @Autowired
    CommentService commentService;

    @Autowired
    CommentsRepository commentsRepository;

    @GetMapping("/post/{id}/{str}")
    public String pcomment(Principal principal, @PathVariable(value = "id")Long id,@PathVariable(value = "str") String str) {
        return commentService.postComment(principal,id,str);
    }

    @GetMapping("/view/{id}")
    public List<Comments> viewComment(@PathVariable(value = "id") Long id) {
        return commentService.view(id);
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(value = "id") Long id) {
        return commentService.delete(id);
    }
}

